import { Timeline } from '../components'

export default [
  {
    path: '/timeline',
    component: Timeline,
    name: 'timeline',
    meta: {
      guest: false,
      needsAuth: true
    }
  }
]

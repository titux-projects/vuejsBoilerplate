import axios from 'axios'
import { setHttpToken } from '../../../helpers'

export const register = ({ dispatch }, { payload, context }) => {
  return axios.post('http://rest-api.loc/users', payload).then((response) => {
    let data = {
      'login': payload.email,
      'password': payload.plainPassword
    }
    return axios.post('http://rest-api.loc/auth-tokens', data).then((response) => {
      dispatch('setToken', response.data.value).then(() => {
        dispatch('fetchUser', response.data)
      })
    })
  }).catch((error) => {
    context.errors = error.response.data.errors.children
  })
}

export const login = ({ dispatch }, { payload, context }) => {
  return axios.post('http://rest-api.loc/auth-tokens', payload).then((response) => {
    dispatch('setToken', response.data.value).then(() => {
      dispatch('fetchUser', response.data)
    })
  }).catch((error) => {
    if (error.response.data.errors) {
      context.errors = {
        login: {
          errors: error.response.data.errors.children.login.errors
        },
        password: {
          errors: error.response.data.errors.children.password.errors
        },
        message: null
      }
      return
    }

    if (error.response.data) {
      context.errors = {
        login: {
          errors: null
        },
        password: {
          errors: null
        },
        message: error.response.data.message
      }
      return
    }
  })
}

export const fetchUser = ({ commit }, authData) => {
  let authTokenId = authData.id
  let userData = authData.user
  commit('setAuthenticated', true)
  commit('setAuthTokenId', authTokenId)
  commit('setUserData', userData)
}

export const setToken = ({ commit, dispatch }, token) => {
  commit('setToken', token)
  setHttpToken(token)
}

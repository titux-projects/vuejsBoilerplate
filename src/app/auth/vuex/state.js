export default {
  user: {
    authenticated: false,
    authTokenId: null,
    data: null
  }
}

import axios from 'axios'
import { isEmpty } from 'lodash'

export const setHttpToken = (token) => {
  if (isEmpty(token)) {
    axios.defaults.headers.common['X-Auth-Token'] = null
  }

  axios.defaults.headers.common['X-Auth-Token'] = token
}

import Vue from 'vue'
import Router from 'vue-router'
import { routes as route } from '../app/index'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: route
})

export default router
